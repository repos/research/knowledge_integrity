"""Model for predicting the need for references in a revision."""

from knowledge_integrity.models.reference_need.model import (
    ClassifyResult,
    ReferenceNeedModel,
    classify,
    load_model,
)


__all__ = ["ClassifyResult", "ReferenceNeedModel", "classify", "load_model"]
