import sqlite3
import tempfile

from datetime import datetime
from typing import Generator, List

import numpy as np
import pytest

from knowledge_integrity.models.reference_risk import (
    ClassifyResult,
    DomainMetadata,
    ReferenceInfo,
    ReferenceRiskModel,
)
from knowledge_integrity.models.reference_risk.domain import (
    UrlInfo,
    extract_cited_urls,
    parse_url_domain,
)
from knowledge_integrity.schema import BaseRevision


@pytest.fixture
def example_reference_risk_model() -> Generator[ReferenceRiskModel, None, None]:
    with tempfile.NamedTemporaryFile(mode="wb") as file:
        conn = sqlite3.connect(file.name)
        cur = conn.execute(
            """CREATE TABLE domains(
            wiki_db,
            domain,
            psl_local,
            psl_enwiki,
            sur_edit_ratio_mean,
            page_distinct_cnt,
            add_user_distinct_cnt,
            snapshot
        )
        """
        )
        data = (
            (
                "testwiki",
                "example.com",
                "Generally reliable",
                "No consensus",
                0.92,
                123,
                3450,
                "2024-07",
            ),
            (
                "testwiki",
                "bad.example.com",
                "Deprecated",
                None,
                0.47,
                20,
                35,
                "2024-07",
            ),
            (
                "anotherwiki",
                "new.example.com",
                None,
                None,
                0.75,
                3,
                6,
                "2024-07",
            ),
        )
        cur.executemany(
            """INSERT INTO domains VALUES(
                :wiki_db,
                :domain,
                :psl_local,
                :psl_enwiki,
                :sur_edit_ratio_mean,
                :page_distinct_cnt,
                :add_user_distinct_cnt,
                :snapshot
            )""",
            data,
        )
        conn.commit()

        model = ReferenceRiskModel(domain_metadata_path=file.name)
        yield model


@pytest.fixture
def example_revision() -> BaseRevision:
    return BaseRevision(
        id=101,
        lang="test",
        text="""
This is a reference.<ref name="example">{{Cite web |url=https://www.example.com#fragment}}</ref>
This is another.<ref>Plunkett, John. [http://bad.example.com/404.html "Example page"]</ref>
This is yet another.<ref>Plunkett, John. [http://missing.example.com/404.html "Missing page"]</ref>
""",  # noqa: E501
        timestamp=datetime(2024, 9, 1),
        bytes=256,
    )


@pytest.mark.parametrize(
    ("url", "expected"),
    (
        (
            "https://www.example.com",
            "www.example.com",
        ),
        (
            "http://www.example.com",
            "www.example.com",
        ),
        (
            "ftp://www.example.com",
            "www.example.com",
        ),
        (
            "https://www.example.com?param=value",
            "www.example.com",
        ),
        (
            "https://www.example.com#fragment",
            "www.example.com",
        ),
        (
            "www.example.com",
            None,
        ),
        (
            "https://web.archive.org/web/20200101010101/http://example.com",
            "example.com",
        ),
        (
            "https://web.archive.org/web/20200101010101/example.com",
            "web.archive.org",
        ),
        (
            "https://web.archive.org/details",
            "web.archive.org",
        ),
        (
            (
                "http://web.archive.org/web/20200101010101/"
                "http://archive.org/web/20200101010101/http://example.com"
            ),
            "example.com",
        ),
        (
            "https://web.archive.org/web/20200101010101/https:///",
            "web.archive.org",
        ),
        (
            "https://www.example.com/web/http://archive.org",
            "www.example.com",
        ),
        (
            "Not-a-url",
            None,
        ),
        (
            "",
            None,
        ),
    ),
)
def test_parse_domain(url: str, expected: str) -> None:
    assert parse_url_domain(url) == expected


@pytest.mark.parametrize(
    ("wikitext", "expected"),
    (
        (
            '<ref name="example">{{Cite web |url=https://www.example.com#fragment |archive-url=https://web.archive.org/web/20200101010101/http://example.com#fragment}}</ref>',  # noqa: E501
            [
                UrlInfo(
                    url="https://www.example.com#fragment",
                    domain="example.com",
                )
            ],
        ),
        (
            '<dummy name="example">{{Cite web |url=https://archive.today/404 |archive-url=https://web.archive.org/web/20200101010101/http://example.com#fragment}}</dummy>',  # noqa: E501
            [],
        ),
        (
            '<ref name="example">{{Cite web |url=https://archive.today/404 |archive-url=https://web.archive.org/web/20200101010101/http://example.com#fragment}}</ref>',  # noqa: E501
            [
                UrlInfo(
                    url="https://archive.today/404",
                    domain="archive.today",
                )
            ],
        ),
        (
            '<ref name="example">{{Cite web |url=https://example,com.uk/404 |archive-url=https://web.archive.org/web/20200101010101/http://example.com}}</ref>',  # noqa: E501
            [
                UrlInfo(
                    url="https://web.archive.org/web/20200101010101/http://example.com",
                    domain="example.com",
                )
            ],
        ),
        (
            """<ref>Plunkett, John. [http://media.guardian.co.uk/site/story/0,14173,1601858,00.html "Sorrell accuses Murdoch of panic buying"], ''[[The Guardian]]'', London, 27 October 2005. Retrieved on 28 October 2005.</ref>""",  # noqa: E501
            [
                UrlInfo(
                    url="http://media.guardian.co.uk/site/story/0,14173,1601858,00.html",  # noqa: E501
                    domain="media.guardian.co.uk",
                )
            ],
        ),
    ),
)
def test_extract_cited_domains(wikitext: str, expected: List[str]) -> None:
    assert extract_cited_urls(wikitext) == expected


def test_reference_risk_model_supported_wikis(
    example_reference_risk_model: ReferenceRiskModel,
) -> None:
    assert example_reference_risk_model.supported_wikis == {"test", "another"}


def test_reference_risk_model_supported_classify(
    example_reference_risk_model: ReferenceRiskModel, example_revision: BaseRevision
) -> None:
    expected = ClassifyResult(
        model_version="2024-07",
        references=[
            ReferenceInfo(
                url="https://www.example.com#fragment",
                domain_name="example.com",
                domain_metadata=DomainMetadata(
                    ps_label_local="Generally reliable",
                    ps_label_enwiki="No consensus",
                    survival_ratio=0.92,
                    page_count=123,
                    editors_count=3450,
                ),
            ),
            ReferenceInfo(
                url="http://bad.example.com/404.html",
                domain_name="bad.example.com",
                domain_metadata=DomainMetadata(
                    ps_label_local="Deprecated",
                    ps_label_enwiki=None,
                    survival_ratio=0.47,
                    page_count=20,
                    editors_count=35,
                ),
            ),
            ReferenceInfo(
                url="http://missing.example.com/404.html",
                domain_name="missing.example.com",
                domain_metadata=None,
            ),
        ],
    )
    actual = example_reference_risk_model.classify(revision=example_revision)

    assert actual == expected
    assert actual.reference_count == 3
    assert actual.survival_ratio is not None
    assert np.isclose(actual.survival_ratio.min, 0.47)
    assert np.isclose(actual.survival_ratio.mean, 0.695)
    assert np.isclose(actual.survival_ratio.median, 0.695)
    assert np.isclose(actual.reference_risk_score, 0.33333)


def test_reference_risk_model_supported_classify_no_references(
    example_reference_risk_model: ReferenceRiskModel, example_revision: BaseRevision
) -> None:
    expected = ClassifyResult(model_version="2024-07", references=[])
    revision = example_revision.model_copy(update=dict(text="No references here."))
    actual = example_reference_risk_model.classify(revision=revision)

    assert actual == expected
    assert actual.reference_count == 0
    assert actual.reference_risk_score == 0
    assert actual.survival_ratio is None


def test_reference_risk_model_supported_classify_no_domain_metadata(
    example_reference_risk_model: ReferenceRiskModel, example_revision: BaseRevision
) -> None:
    expected = ClassifyResult(
        model_version="2024-07",
        references=[
            ReferenceInfo(
                url="https://www.archive.org",
                domain_name="archive.org",
                domain_metadata=None,
            ),
        ],
    )
    revision = example_revision.model_copy(
        update=dict(
            text='This is a reference.<ref name="example">{{Cite web |url=https://www.archive.org}}</ref>'  # noqa: E501
        )
    )
    actual = example_reference_risk_model.classify(revision=revision)

    assert actual == expected
    assert actual.reference_count == 1
    assert actual.reference_risk_score == 0
    assert actual.survival_ratio is None


def test_reference_risk_model_classify_unsupported_wiki(
    example_reference_risk_model: ReferenceRiskModel, example_revision: BaseRevision
) -> None:
    revision = example_revision.model_copy(update=dict(lang="missing"))
    with pytest.raises(ValueError, match="lang missing is not supported."):
        example_reference_risk_model.classify(revision=revision)
