import re

from dataclasses import dataclass
from enum import Enum, unique
from typing import Callable, List, Optional, Pattern, Tuple, Union

from typing_extensions import Literal, TypeAlias


CLAIM_EDIT_RE = re.compile(
    r"""
    ^/\*[ ]                                     # opening /*
    wbsetclaim(?:-create|-update|value):[\d|]+  # edit type e.g. wbsetclaim-create:2||1
    [ ]\*/[ ]                                   # closing */
    \[\[Property:(\w+)\]\]:[ ]                  # claim property e.g. [[Property:P31]]:
    \[\[(\w+)\]\]                               # target QID e.g. [[Q123]]
    .*$                                         # optional comment
    """,
    re.VERBOSE,
)
DESCRIPTION_EDIT_RE = re.compile(
    r"""
    ^/\*[ ]                            # opening /*
    wbsetdescription(?:-add|-set):1\|  # edit type e.g. wbsetdescription-add:1|
    ([\w-]+)                           # description wiki code e.g en or zh-yue
    [ ]\*/[ ]                          # closing */
    (.+)$                              # the description
    """,
    re.VERBOSE,
)


@unique
class EditKind(Enum):
    CLAIM = "claim"
    DESCRIPTION = "description"


@dataclass(frozen=True)
class ClaimEdit:
    prop: str
    target: str
    kind: Literal[EditKind.CLAIM] = EditKind.CLAIM


@dataclass(frozen=True)
class DescriptionEdit:
    lang: str
    description: str
    kind: Literal[EditKind.DESCRIPTION] = EditKind.DESCRIPTION


Edit: TypeAlias = Union[ClaimEdit, DescriptionEdit]


def parse_edit(summary: str) -> Optional[Edit]:
    """Returns an `Edit` by parsing wikidata autosummaries
    using regexes. Note that for complex cases not covered
    by these regexes, this function would simply return `None`.
    """
    cases: List[Tuple[Pattern, Callable]] = [
        (CLAIM_EDIT_RE, lambda prop, target: ClaimEdit(prop, target)),
        (DESCRIPTION_EDIT_RE, lambda lang, desc: DescriptionEdit(lang, desc)),
    ]
    for regex, action in cases:
        match = regex.match(summary)
        if match is not None:
            return action(*match.groups())

    return None
