import sqlite3

from dataclasses import dataclass
from functools import cached_property
from os import PathLike
from typing import Any, Dict, List, Optional, Sequence, Set, Union

import numpy as np
import packaging
import packaging.version

from typing_extensions import TypeAlias

from knowledge_integrity.featureset import ContentFeatures, FeatureSource, get_features
from knowledge_integrity.models.reference_risk.domain import extract_cited_urls
from knowledge_integrity.schema import BaseRevision


MODEL_VERSION = "2024-07"
"""Minimum version of domain metadata that is compatible with the current code."""


@dataclass(frozen=True)
class DomainMetadata:
    ps_label_local: Optional[str]
    ps_label_enwiki: Optional[str]
    survival_ratio: float
    page_count: int
    editors_count: int

    @property
    def is_risky(self) -> bool:
        return self.ps_label_local in ("Deprecated", "Blacklisted")


@dataclass(frozen=True)
class ReferenceInfo:
    url: str
    domain_name: str
    # domain_metadata is `None` when domain_name
    # is absent from the domain metadata sqlite db.
    domain_metadata: Optional[DomainMetadata]


@dataclass(frozen=True)
class ReferenceSurvival:
    min: float
    mean: float
    median: float


@dataclass(frozen=True)
class ClassifyResult:
    model_version: str
    references: List[ReferenceInfo]

    @property
    def reference_count(self) -> int:
        return len(self.references)

    @cached_property
    def survival_ratio(self) -> Optional[ReferenceSurvival]:
        survival_ratios = [
            ref.domain_metadata.survival_ratio
            for ref in self.references
            if ref.domain_metadata
        ]
        if not survival_ratios:
            return None

        return ReferenceSurvival(
            min=float(np.min(survival_ratios)),
            mean=float(np.mean(survival_ratios)),
            median=float(np.median(survival_ratios)),
        )

    @cached_property
    def reference_risk_score(self) -> float:
        if not self.references:
            return 0

        risky_domain_count = sum(
            ref.domain_metadata.is_risky
            for ref in self.references
            if ref.domain_metadata
        )
        return risky_domain_count / len(self.references)


def extract_features(revision: BaseRevision, features: Sequence[str]) -> Dict[str, Any]:
    content_features = ContentFeatures(revision)
    feature_sources = (FeatureSource(source=content_features),)
    return get_features(
        features=features,
        feature_sources=feature_sources,
        transformer=lambda feature_dict: dict(
            references=extract_cited_urls(feature_dict["wikitext"])
        ),
    )


StrOrBytesPath: TypeAlias = Union[str, bytes, PathLike]


class ReferenceRiskModel:
    """Model for predicting the risk scores of references in a revision."""

    def __init__(self, domain_metadata_path: StrOrBytesPath) -> None:
        self._conn = sqlite3.connect(database=domain_metadata_path)
        self._conn.row_factory = sqlite3.Row

        (snapshot,) = self._conn.execute("SELECT snapshot FROM domains").fetchone()
        if packaging.version.parse(MODEL_VERSION) > packaging.version.parse(snapshot):
            self._conn.close()
            raise ValueError(
                f"The domain metadata snapshot is {snapshot} "
                f"but the minimum required version is {MODEL_VERSION}."
            )
        self.model_version = snapshot

    @cached_property
    def supported_wikis(self) -> Set[str]:
        results = self._conn.execute("SELECT DISTINCT wiki_db FROM domains").fetchall()
        wiki_dbs = set(wiki_db.replace("wiki", "") for wiki_db, *_ in results)
        return wiki_dbs

    def _fetch_domain_metadata(
        self, wiki_db: str, domains: Sequence[str]
    ) -> Dict[str, DomainMetadata]:
        domain_placeholder = ", ".join(("?",) * len(domains))
        results = self._conn.execute(
            f"""SELECT * FROM domains
            WHERE wiki_db = ? AND domain IN ({domain_placeholder})
            """,
            (wiki_db, *domains),
        )
        domain_metadata = {
            result["domain"]: DomainMetadata(
                survival_ratio=result["sur_edit_ratio_mean"],
                page_count=result["page_distinct_cnt"],
                editors_count=result["add_user_distinct_cnt"],
                ps_label_local=result["psl_local"],
                ps_label_enwiki=result["psl_enwiki"],
            )
            for result in results
        }
        return domain_metadata

    def classify(self, revision: BaseRevision) -> ClassifyResult:
        if revision.lang not in self.supported_wikis:
            raise ValueError(f"lang {revision.lang} is not supported.")

        revision_features = extract_features(
            revision=revision, features=["references", "wiki_db"]
        )
        references = revision_features["references"]
        if not references:
            return ClassifyResult(
                model_version=self.model_version,
                references=[],
            )

        domain_metadata = self._fetch_domain_metadata(
            wiki_db=revision_features["wiki_db"],
            domains=[ref.domain for ref in references],
        )
        reference_features = [
            ReferenceInfo(
                url=ref.url,
                domain_name=ref.domain,
                domain_metadata=domain_metadata.get(ref.domain),
            )
            for ref in references
        ]
        return ClassifyResult(
            model_version=self.model_version,
            references=reference_features,
        )
