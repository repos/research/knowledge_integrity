from typing import List

import pytest

from knowledge_integrity.models.reference_need.utils import (
    SentenceInput,
    extract_sentences,
    wikitext_to_plaintext,
)


@pytest.mark.parametrize(
    ("wikitext", "lang", "expected"),
    (
        pytest.param(
            (
                "This line contains a reference."
                "<ref>A reference: https://example.com/url-string</ref>"
            ),
            "en",
            "This line contains a reference.[REF_FN_TOKEN]",
            id="reftag",
        ),
        pytest.param(
            (
                "\n\n==References==\n{{reflist}}\n\n===Works===\n"
                "The [[Foo, Bar|Foo]]."
                "<ref>{{Baz (3rd edition)|title=Foo, Bar |volume=1}}</ref>"
            ),
            "en",
            "\n\n\n\n\n\nThe Foo.[REF_FN_TOKEN]",
            id="reflist",
        ),
        pytest.param(
            "Still more article text.{{sfn|Smith|2020|p=26}}",
            "en",
            "Still more article text.[REF_FN_TOKEN]",
            id="template-sfn",
        ),
        pytest.param(
            "Still more article text.{{sfnp|Smith|2020|p=26}}",
            "en",
            "Still more article text.[REF_FN_TOKEN]",
            id="template-sfn-variation",
        ),
        pytest.param(
            "Still more article text.{{Sfn|Smith|2020|p=26}}",
            "en",
            "Still more article text.[REF_FN_TOKEN]",
            id="template-sfn-uppercase",
        ),
        pytest.param(
            "Still more article text.{{snf|Smith|2020|p=26}}",
            "en",
            "Still more article text.[REF_FN_TOKEN]",
            id="template-snf",
        ),
        pytest.param(
            "Still more article text.<ref>{{harv|Smith|2020|p=26}}</ref>",
            "en",
            "Still more article text.[REF_FN_TOKEN]",
            id="template-harv",
        ),
        pytest.param(
            "Still more article text.{{harv|Smith|2020|p=26}}",
            "en",
            "Still more article text.[REF_FN_TOKEN]",
            id="template-harv-deprecated",
        ),
        pytest.param(
            "Still more article text.<ref>{{harvnb|Smith|2020|p=26}}</ref>",
            "en",
            "Still more article text.[REF_FN_TOKEN]",
            id="template-harv-variation",
        ),
        pytest.param(
            "Still more article text.{{r|RefName|p=22}}",
            "en",
            "Still more article text.[REF_FN_TOKEN]",
            id="template-r",
        ),
        pytest.param(
            "Still more article text.{{rr|RefName|p=22}}",
            "en",
            "Still more article text.",
            id="template-begins-with-r",
        ),
        pytest.param(
            'She said: "{{lang|fr|Je suis française.}}"',
            "en",
            'She said: "Je suis francaise."',
            id="template-lang",
        ),
        pytest.param(
            'She said: "{{Lang|fr|Je suis française.}}"',
            "en",
            'She said: "Je suis francaise."',
            id="template-lang-uppercase",
        ),
        pytest.param(
            "{{Use dmy dates|date=April 2017}}\n",
            "en",
            "\n",
            id="unimportant-template",
        ),
        pytest.param(
            """Here is a table:
{| class="wikitable"
|+ Caption text
|-
! Header text !! Header text !! Header text
|-
| Example || Example || Example
|-
| Example || Example || Example
|}""",
            "en",
            "Here is a table:\n",
            id="table",
        ),
    ),
)
def test_convert_wikitext_to_plaintext(wikitext: str, lang: str, expected: str) -> None:
    actual = wikitext_to_plaintext(wikitext, lang)
    assert actual == expected


@pytest.mark.parametrize(
    ("wikitext", "lang", "expected"),
    (
        pytest.param(
            (
                "They've been M.I.A. lately.<ref>A reference: "
                "https://example.com/url-string</ref>"
            ),
            "en",
            [
                SentenceInput(
                    lang_code="en",
                    section_name="main_section",
                    sent="They've been M.I.A. lately.",
                    prev_sent="",
                    next_sent="",
                    label="1",
                )
            ],
            id="abbreviation-ending-in-period",
        ),
        pytest.param(
            (
                "This a simple sentence (maybe. (not.)).<ref>Reference 1</ref> "
                "Was that simple?<ref>Reference 2</ref> Possibly."
            ),
            "en",
            [
                SentenceInput(
                    lang_code="en",
                    section_name="main_section",
                    sent="This a simple sentence (maybe. (not.)).",
                    prev_sent="",
                    next_sent="Was that simple? Possibly.",
                    label="1",
                ),
                SentenceInput(
                    lang_code="en",
                    section_name="main_section",
                    sent="Was that simple? Possibly.",
                    prev_sent="This a simple sentence (maybe. (not.)).",
                    next_sent="",
                    label="1",
                ),
            ],
            id="period-between-other-punctuation",
        ),
        pytest.param(
            "They've been M.I.A. lately.",
            "en",
            [
                SentenceInput(
                    lang_code="en",
                    section_name="main_section",
                    sent="They've been M.I.A. lately.",
                    prev_sent="",
                    next_sent="",
                    label="0",
                )
            ],
            id="wikitext-without-refs",
        ),
    ),
)
def test_extract_sentences(
    wikitext: str, lang: str, expected: List[SentenceInput]
) -> None:
    actual = extract_sentences(lang_code=lang, wikitext=wikitext)
    assert actual == expected
