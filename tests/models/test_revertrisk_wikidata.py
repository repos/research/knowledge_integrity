from typing import Optional

import pytest

from knowledge_integrity.models.revertrisk_wikidata.edit import (
    ClaimEdit,
    DescriptionEdit,
    Edit,
    parse_edit,
)


@pytest.mark.parametrize(
    ("summary", "expected"),
    [
        (
            "/* wbsetclaim-create:2||1 */ [[Property:P11]]: [[Q123]]",
            ClaimEdit(prop="P11", target="Q123"),
        ),
        (
            "/* wbsetclaim-create:2||1 */ [[Property:P11]]: [[Q123]], optional comment",
            ClaimEdit(prop="P11", target="Q123"),
        ),
        (
            "/* wbsetclaim-update:2||1|1 */ [[Property:P11]]: [[Q123]]",
            ClaimEdit(prop="P11", target="Q123"),
        ),
        (
            "/* wbsetclaim-update:2||1|1 */ [[Property:P11]]: [[Q123]], opt., comment.",
            ClaimEdit(prop="P11", target="Q123"),
        ),
        (
            "/* wbsetclaimvalue:1| */ [[Property:P11]]: [[Q123]]",
            ClaimEdit(prop="P11", target="Q123"),
        ),
        (
            "/* wbsetclaim-create:2||1 */ [[Property:P11]]: Title",
            None,
        ),
        (
            "/* wbsetclaim-create:2||1 */ [[Property:P11]]: Title, by [[Q123]]",
            None,
        ),
        (
            "/* wbsetclaim-remove:2||1 */ [[Property:P11]]: [[Q123]]",
            None,
        ),
        (
            "/* wbsetdescription-add:1|en */ example description, end",
            DescriptionEdit(lang="en", description="example description, end"),
        ),
        (
            "/* wbsetdescription-add:1|zh-yue */ example description, end",
            DescriptionEdit(lang="zh-yue", description="example description, end"),
        ),
        (
            "/* wbsetdescription-remove:1|ar */ example description",
            None,
        ),
    ],
)
def test_parse_edit(summary: str, expected: Optional[Edit]) -> None:
    assert parse_edit(summary) == expected
