from typing import List

import transformers

from knowledge_integrity.models.reference_need.utils import SentenceInput


def predict_sentences_scores(
    model: transformers.Pipeline,
    inputs: List[SentenceInput],
    truncation: bool = True,
    batch_size: int = 1,
) -> List[float]:
    formatted_inputs = [
        {
            "text": f"{sent.lang_code}[SEP]{sent.section_name}",
            "text_pair": f"{sent.sent}[SEP]{sent.next_sent}[SEP]{sent.prev_sent}",
        }
        for sent in inputs
    ]
    raw_predictions = model(
        formatted_inputs,
        top_k=2,
        truncation=truncation,
        batch_size=batch_size,
    )
    sentences_scores = [
        [score["score"] for score in raw_score if score["label"] == "LABEL_1"][0]
        for raw_score in raw_predictions
    ]

    return sentences_scores


def compute_rn_score(n_cited: int, predicted_cited: List[bool]) -> float:
    """
    RN is computed as a proportion:
    <n_missing_citation> / (<n_missing_citation> + <n_cited>)
    """
    n_missing_citation = sum(predicted_cited)
    if n_missing_citation == 0:
        return 0
    return n_missing_citation / (n_missing_citation + n_cited)
