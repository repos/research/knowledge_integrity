"""Model for predicting the risk scores of references in a revision.

Model card: https://meta.wikimedia.org/wiki/Machine_learning_models/Proposed/Language-agnostic_reference_risk
Original implementation: https://gitlab.wikimedia.org/repos/research/reference-quality/-/tree/b0f83407ac3873b74a8d84a42d6d56b189db1df6/reference_quality/models/ref_risk
"""  # noqa: E501

from knowledge_integrity.models.reference_risk.model import (
    ClassifyResult,
    DomainMetadata,
    ReferenceInfo,
    ReferenceRiskModel,
)


__all__ = ["ClassifyResult", "DomainMetadata", "ReferenceInfo", "ReferenceRiskModel"]
