import datetime

from typing import Any, Dict

import mwapi
import pytest
import pytest_asyncio

from knowledge_integrity.mediawiki import (
    get_page,
    get_parent_revision,
    get_revision,
    get_user,
)
from knowledge_integrity.schema import Page, ParentRevision, Revision, User


@pytest.fixture(scope="module")
def vcr_config() -> Dict[str, Any]:
    return dict(
        cassette_library_dir="tests/fixtures/cassettes",
        record_mode="new_episodes",
        match_on=["uri", "method"],
    )


@pytest_asyncio.fixture(scope="module")
async def async_session() -> mwapi.AsyncSession:
    return mwapi.AsyncSession(host="https://en.wikipedia.org")


@pytest.mark.vcr
@pytest.mark.asyncio
async def test_get_user(async_session: mwapi.AsyncSession) -> None:
    user_id = 4587601
    user = await get_user(async_session, user_id)

    assert isinstance(user, User)

    for attr, expected in [
        (user.id, user_id),
        (user.name, "Catrope"),
        (user.groups, ["*", "user", "autoconfirmed"]),
        (user.editcount, 393),
        (
            user.registration_timestamp,
            datetime.datetime(2007, 6, 7, 16, 36, 3, tzinfo=datetime.timezone.utc),
        ),
    ]:
        assert attr == expected


@pytest.mark.vcr
@pytest.mark.asyncio
async def test_get_user_anonymous(async_session: mwapi.AsyncSession) -> None:
    user_id = 0
    user = await get_user(async_session, user_id)

    assert isinstance(user, User)

    for attr, expected in [
        (user.id, user_id),
        (user.groups, []),
        (user.editcount, 0),
    ]:
        assert attr == expected

    for attr, expected in [
        (user.name, None),
        (user.registration_timestamp, None),
    ]:
        assert attr is expected


@pytest.mark.vcr
@pytest.mark.asyncio
async def test_get_page(async_session: mwapi.AsyncSession) -> None:
    page_id = 211652
    page = await get_page(async_session, page_id)

    assert isinstance(page, Page)
    assert page.id == page_id
    assert page.first_edit_timestamp == datetime.datetime(
        2003, 4, 16, 22, 7, 27, tzinfo=datetime.timezone.utc
    )
    assert page.title == "Constraint satisfaction problem"


@pytest.mark.vcr
@pytest.mark.asyncio
async def test_get_parent_revision(async_session: mwapi.AsyncSession) -> None:
    lang = "en"
    rev_id = 1094938018
    parent = await get_parent_revision(async_session, rev_id, lang)

    assert isinstance(parent, ParentRevision)

    for attr, expected in [
        (parent.id, rev_id),
        (parent.bytes, 20464),
        (parent.lang, lang),
        (parent.comment, ""),
        (
            parent.timestamp,
            datetime.datetime(2022, 6, 25, 13, 1, 2, tzinfo=datetime.timezone.utc),
        ),
    ]:
        assert attr == expected

    for fn, arg, ret in [
        (len, parent.categories, 2),
        (len, parent.headings, 17),
        (len, parent.media, 18),
        (len, parent.links, 90),
        (len, parent.wikilinks, 76),
        (len, parent.references, 7),
        (len, parent.tags, 4),
    ]:
        assert fn(arg) == ret


@pytest.mark.vcr
@pytest.mark.asyncio
async def test_get_revision(async_session: mwapi.AsyncSession) -> None:
    lang = "en"
    rev_id = 1095483351
    revision = await get_revision(async_session, rev_id, lang)

    assert isinstance(revision, Revision)

    for attr, expected in [
        (revision.id, rev_id),
        (revision.bytes, 20533),
        (revision.lang, lang),
        (revision.comment, "Added {{Manifolds}}"),
        (
            revision.timestamp,
            datetime.datetime(2022, 6, 28, 16, 48, 52, tzinfo=datetime.timezone.utc),
        ),
    ]:
        assert attr == expected

    for fn, arg, ret in [
        (len, revision.categories, 4),
        (len, revision.headings, 17),
        (len, revision.media, 18),
        (len, revision.links, 92),
        (len, revision.wikilinks, 76),
        (len, revision.references, 7),
        (len, revision.tags, 1),
    ]:
        assert fn(arg) == ret

    assert isinstance(revision.user, User)
    assert isinstance(revision.page, Page)
    assert isinstance(revision.parent, ParentRevision)
