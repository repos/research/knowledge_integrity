# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.8.3] - 2024-09-26

### Added

- (!48) Added `ReferenceRiskModel` model for predicting the risk score of references in a revision.

## [0.8.2] - 2024-08-30

### Changed

- (!46) Expand list of sections to skip when making predictions with section titles from more wikis.
- (!46) Max length of input passed to the reference need model is now fixed to 128 tokens.

## [0.8.1] - 2024-08-26

### Added

- (!42) Added `ReferenceNeedModel` model for predicting the need of references in an article.

## [0.8.0] - 2024-05-31

### Changed

- (!35) `User` schema has been adapted to handle temporary users.
- (!40) BREAKING: `models.revertrisk.predict` now expects a batch of revisions instead of a single revision.

## [0.7.0] - 2024-04-26

### Added

- (!37) `QualityFeatures` now falls back to default values if pre-computed thresholds don't exist for a wiki.

### Changed

- (!37) BREAKING: `UnsupportedWikiException` has been removed.

## [0.6.0] - 2024-01-19

### Added

- (!31) `catboost` based models `MultilingualRevertRiskModel` and `RevertRiskWikidataModel`
  now allow setting the number of threads to use for prediction.

### Changed

- (!30) BREAKING: `CurrentRevision` has been renamed to `Revision`.
- (!30) BREAKING: MediaWiki API client code, i.e. functions like `get_revision`, have been moved into the `knowledge_integrity.mediawiki` module.

### Fixed

- (!30) Improved error handling for MW API requests.

## [0.5.0] - 2023-11-17

### Changed

- (!26) BREAKING: Support for Python 3.7 has been dropped after its EOL in June 2023.
- (!28) BREAKING: `RevertRiskModel` has now been bumped to version 3.0. Any serialized models with an older version number will raise a warning when loaded.

## [0.4.0] - 2023-09-29

### Added

- (!23) Updated `MAX_FEATURE_VALS` to include previously missing wikis.

### Changed

- (!23) BREAKING: `QualityFeatures` now raises an `UnsupportedWikiException` for wikis that we don't have quality feature values for.
- (!23) BREAKING: `RevertRiskModel` has now been bumped to version 2.0. Any serialized models with an older version number will raise a warning when loaded.

## [0.3.0] - 2023-07-27

### Added

- (!16) Added new model for predicting reverts on Wikidata

### Changed

- (!16) BREAKING: `REVISION_SCHEMA` now requires `user_name`.
- (!17) Replaced `EditTypes` from `mwedittypes` with the new `SimpleEditTypes` in Revertrisk Multilingual for improved stability and performance.

## [0.2.0] - 2022-12-14

### Added

- (!7) Added content oriented Knowledge Integrity model for predicting reverts.

### Changed

- (!7) BREAKING: `REVISION_SCHEMA` now requires `page_title`, `rev_tags` and `parent_tags`.

## [0.1.1] - 2022-12-14

### Added

- (!5) Package is now `PEP-561` compliant

## [0.1.0] - 2022-10-25

First release.
