import logging
import pathlib
import sys

from dataclasses import dataclass
from typing import Any, Dict, List, Sequence

import joblib  # type: ignore
import transformers

from knowledge_integrity.featureset import ContentFeatures, FeatureSource, get_features
from knowledge_integrity.models.reference_need.bert import (
    compute_rn_score,
    predict_sentences_scores,
)
from knowledge_integrity.models.reference_need.utils import extract_sentences
from knowledge_integrity.schema import BaseRevision


MODEL_VERSION: int = 0
MODEL_FEATURES = ["inputs", "cited"]
DECISION_THRESHOLD = 0.5


@dataclass
class ClassifyResult:
    rn_score: float


@dataclass
class ReferenceNeedModel:
    model_version: int
    classifier: transformers.Pipeline
    supported_wikis: List[str]


def _transformed_ref_need_features(features: Dict[str, Any]) -> Dict[str, Any]:
    sentences = extract_sentences(
        features["wiki_db"].replace("wiki", ""), features["wikitext"]
    )
    inputs = [sentence for sentence in sentences if sentence.label == "0"]
    n_cited = len(sentences) - len(inputs)
    return dict(
        inputs=inputs,
        cited=n_cited,
    )


def extract_features(revision: BaseRevision, features: Sequence[str]) -> Dict[str, Any]:
    # groups of features required for classification model
    content_features = ContentFeatures(revision)
    feature_sources = (FeatureSource(source=content_features),)

    return get_features(feature_sources, features, _transformed_ref_need_features)


def load_model(model_path: pathlib.Path) -> ReferenceNeedModel:
    # ensure that joblib can find the model in global symbols when unpickling
    sys.modules["__main__"].ReferenceNeedModel = ReferenceNeedModel  # type: ignore

    with open(model_path, "rb") as f:
        model: ReferenceNeedModel = joblib.load(f)

    if model.model_version != MODEL_VERSION:
        logging.warn(
            "Serialized model version %d does not match current model version %d",
            model.model_version,
            MODEL_VERSION,
        )
    return model


def classify(model: ReferenceNeedModel, revision: BaseRevision) -> ClassifyResult:
    ref_need_features = extract_features(revision, MODEL_FEATURES)
    sentence_scores = predict_sentences_scores(
        model.classifier, ref_need_features["inputs"]
    )
    sentence_labels = [bool(score > DECISION_THRESHOLD) for score in sentence_scores]
    rn_score = compute_rn_score(ref_need_features["cited"], sentence_labels)

    return ClassifyResult(
        # convert numpy values to make them serializable
        rn_score=float(rn_score),
    )
