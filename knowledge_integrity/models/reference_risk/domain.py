import re
import urllib.parse

from dataclasses import dataclass
from typing import List, Optional

import regex


# Regular expressions in this module have been taken from:
# https://gitlab.wikimedia.org/repos/research/reference-quality/-/blob/b0f83407ac3873b74a8d84a42d6d56b189db1df6/reference_quality/utils.py.
# Some have been adapted to be more robust against unicode input.
# While some of these regexes can be replaced by using mwparser, it was found to be
# considerably slower which might affect the latency of the model.
SCHEME_RE = r"(?:ftp|http)s?://"
URL_ALLOWED_CHARS_RE = r"[^\s|{}\[\].<>]"


def is_archive_domain(domain: str) -> bool:
    archive_domains = (
        "archive-it.org",
        "archive.is",
        "archive.org",
        "archive.today",
        "web.archive.org",
    )
    return any(archive_domain in domain for archive_domain in archive_domains)


def parse_url_domain(url: str) -> Optional[str]:
    """Extract the domain from a URL.

    If the domain is an archive domain (e.g. web.archive.org), it tries to
    recursively parse the original URL from the path, given that it contains
    a scheme. Returns `None` if the domain cannot be parsed.
    """
    try:
        parsed_url = urllib.parse.urlparse(urllib.parse.unquote(url))
        domain = parsed_url.netloc
    except ValueError:
        return None

    if is_archive_domain(domain):
        match = re.search(rf"{SCHEME_RE}.+", parsed_url.path, flags=re.VERBOSE)
        if match:
            original_url = match.group()
            # Occasionally, an archived link can point to another
            # archived link. Recursive parsing handles that.
            original_domain = parse_url_domain(original_url)
            domain = original_domain if original_domain else domain

    return domain if domain else None


def normalize_domain(domain: str) -> Optional[str]:
    subdomain_re = r"""
    ^www  # www in the beginning
    \d*   # optional number
    \.    # period
    """
    sld_with_tld_re = r"""
    (?:[\p{Dash}\w]{1,64}\.)+  # second level domain i.e. 'example.' or 'example.co.'
    [\p{Dash}\w]{1,64}\.?      # top level domain i.e. 'org' or 'uk'
    """
    stripped_domain = re.sub(subdomain_re, "", domain.lower(), flags=re.VERBOSE)
    # Also strip the port and any other invalid chars captured during parsing.
    # NOTE: This uses `regex` instead of `re` because the latter seems to miss valid
    # characters sometimes even though they're listed as alphanumeric in the unicode db.
    domain_match = regex.match(sld_with_tld_re, stripped_domain, flags=regex.VERBOSE)
    return domain_match.group() if domain_match else None


def parse_references(wikitext: str) -> List[str]:
    reference_re = r"""
    <ref[^/]*?>  # opening tag with optional attr(s)
    (.*?)        # enclosed contents
    </ref>       # closing tag
    """
    references = re.findall(reference_re, wikitext, flags=re.DOTALL | re.VERBOSE)
    return references


@dataclass(frozen=True)
class UrlInfo:
    url: str
    domain: str


def extract_cited_urls(wikitext: str) -> List[UrlInfo]:
    """Extract domains of URLs used as citations in the provided wikitext.

    Parses <ref> tags out of the wikitext. The domain is then parsed from
    the first non-archive (e.g. wayback archive) link within the reference.
    If no non-archive links are found, the first one is returned.
    """
    url_re = rf"""
    \b{SCHEME_RE}
    {URL_ALLOWED_CHARS_RE}+         # sub-domain
    (?:\.{URL_ALLOWED_CHARS_RE}+)*  # SLD and TLD
    """

    def extract_domain(link: str) -> Optional[str]:
        domain = parse_url_domain(link)
        return normalize_domain(domain) if domain else None

    cited_urls: List[UrlInfo] = []
    for reference in parse_references(wikitext):
        reference_urls = re.findall(url_re, reference, flags=re.VERBOSE)
        if not reference_urls:
            continue

        url_info = next(
            (
                UrlInfo(url=url, domain=extracted_domain)
                for url in reference_urls
                if (extracted_domain := extract_domain(url))
            ),
            None,
        )
        if url_info:
            cited_urls.append(url_info)

    return cited_urls
