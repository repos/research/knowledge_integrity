import re

from datetime import datetime
from functools import cached_property
from typing import Any, List, Optional

import pydantic

from typing_extensions import Self

from knowledge_integrity import constants


HEADING_RE = re.compile(
    r"""
    (={2,})  # markup
    (.*?)    # heading
    (={2,})  # markup
    """,
    re.VERBOSE,
)

LINK_RE = re.compile(
    r"""
    (?<=\[\[)  # starts with [[ but not included in match
    .*?        # enclosed text
    (?=]])     # ends with ]] but not included in match
    """,
    re.VERBOSE,
)

MEDIA_NAME_PREFIX = r"""
(?:
      =   # equals sign if in a template
    | :   # or a colon if of the form file:foo.jpg
    | \n  # or newline if in a gallery tag
)
"""
ALLOWED_MEDIA_NAME_CHARACTERS = r"""
    # any character excluding carriage return, newline,
    # hash, angle brackets, square brackets, vertical bar,
    # colon, braces and forward slash
    [^\r\n\#\<\>\[\]\|:\{\}/]
"""
MEDIA_RE = re.compile(
    rf"""
    {MEDIA_NAME_PREFIX}                 # prefix
    ({ALLOWED_MEDIA_NAME_CHARACTERS}+)  # filename
    \.(\w+)                             # extension with period
    """,
    re.VERBOSE,
)

REFERENCE_RE = re.compile(
    r"""
    <ref              # open ref tag
    [^/>]*            # optional reference name i.e. name="foo"
    (?:
          />          # self close if repeated reference
        | >.*?</ref>  # or angle bracket and closing tag with text in between
    )
    """,
    re.IGNORECASE | re.VERBOSE,
)


class InvalidJSONError(Exception):
    pass


class CustomBaseModel(pydantic.BaseModel):
    @classmethod
    def from_json(cls, json_data: str) -> Self:
        try:
            deserialized_model = cls.model_validate_json(json_data)
            return deserialized_model
        except pydantic.ValidationError as e:
            # If pydantic fails to validate json
            # data raise a custom exception
            raise InvalidJSONError from e


class User(CustomBaseModel):
    # id is 0 when the user is anonymous
    id: int = 0
    name: Optional[str] = None
    editcount: int = 0
    groups: List[str] = pydantic.Field(default_factory=lambda: [])
    # registration timestamp can be None when
    # the user is anonymous
    registration_timestamp: Optional[datetime] = pydantic.Field(
        default=None,
        validation_alias=pydantic.AliasChoices(
            "registration_timestamp", "registration"
        ),
    )

    @pydantic.model_validator(mode="before")
    @classmethod
    def handle_temp_user(cls, data: Any) -> Any:
        if isinstance(data, dict) and "temp" in data.get("groups", []):
            # NOTE: Temp users get treated as ip editors at the moment which
            # means any data that they have that an ip editor does not gets
            # discarded. See https://phabricator.wikimedia.org/T352839.
            return {"id": 0}
        return data


class Page(CustomBaseModel):
    id: int
    first_edit_timestamp: datetime
    title: str


class BaseRevision(CustomBaseModel):
    id: int
    lang: str
    text: str
    comment: str = ""
    timestamp: datetime
    bytes: int = pydantic.Field(validation_alias=pydantic.AliasChoices("bytes", "size"))
    tags: List[str] = pydantic.Field(default_factory=lambda: [])

    # We need to add type: ignore[misc] next to the computed_field decorator
    # to suppress this mypy false positive: https://github.com/python/mypy/issues/1362
    @pydantic.computed_field  # type: ignore[misc]
    @cached_property
    def links(self) -> List[str]:
        # split to get only the title from matches of the form
        # "title | (optional) alias"
        links = [m.split("|", maxsplit=1)[0] for m in LINK_RE.findall(self.text)]
        return links

    @pydantic.computed_field  # type: ignore[misc]
    @cached_property
    def categories(self) -> List[str]:
        # split and filter to only get links of the form "Category:Foo"
        split_links = (link.split(":", maxsplit=1)[0] for link in self.links)
        categories = [
            link
            for link in split_links
            if link in constants.CATEGORY_PREFIXES
            or link in constants.CATEGORY_ALIASES.get(self.lang, [])
        ]
        return categories

    @pydantic.computed_field  # type: ignore[misc]
    @cached_property
    def headings(self) -> List[str]:
        # only get headings of level 2 and 3
        headings = [
            heading.strip()
            for l_eq, heading, r_eq in HEADING_RE.findall(self.text)
            if l_eq == r_eq and 2 <= len(l_eq) <= 3
        ]
        return headings

    @pydantic.computed_field  # type: ignore[misc]
    @cached_property
    def media(self) -> List[str]:
        media = [
            f"{name}.{extension}".strip()
            for name, extension in MEDIA_RE.findall(self.text)
            if extension.lower() in constants.MEDIA_EXTENSIONS
        ]
        return media

    @pydantic.computed_field  # type: ignore[misc]
    @cached_property
    def references(self) -> List[str]:
        references = REFERENCE_RE.findall(self.text)
        return references

    @pydantic.computed_field  # type: ignore[misc]
    @cached_property
    def wikilinks(self) -> List[str]:
        wikilinks = [link for link in self.links if ":" not in link]
        return wikilinks


class ParentRevision(BaseRevision):
    pass


class Revision(BaseRevision):
    parent: ParentRevision
    page: Page
    user: User
