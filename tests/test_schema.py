import datetime
import json

from typing import Any, Dict

import pytest

from knowledge_integrity.schema import (
    InvalidJSONError,
    Page,
    ParentRevision,
    Revision,
    User,
)


@pytest.fixture
def revision_data() -> Dict[str, Any]:
    return {
        "id": 1234,
        "bytes": 2800,
        "comment": "Added category baz.",
        "text": """This is a lead.
                == Section I ==
                Section I body. {{and a|template}}
                === Section I.A ===
                Section I.A [[body]].
                === Section I.B ===
                Section I.B body.

                [[Category:bar]]
                [[Category:baz]]
            """,
        "timestamp": "2022-02-15T04:30:00Z",
        "tags": [],
        "parent": {
            "id": 1200,
            "bytes": 2600,
            "comment": "Added section I.B",
            "text": """This is a lead.
                == Section I ==
                Section I body. {{and a|template}}
                === Section I.A ===
                Section I.A [[body]].
                === Section I.B ===
                Section I.B body.

                [[Category:bar]]
            """,
            "timestamp": "2021-01-01T02:00:00Z",
            "tags": [],
            "lang": "en",
        },
        "user": {
            "id": 4567,
            "name": "Athena",
            "editcount": 30,
            "groups": ["rollbacker"],
            "registration_timestamp": "2020-06-01T10:05:05Z",
        },
        "page": {
            "id": 1008,
            "title": "this is a title",
            "first_edit_timestamp": "2018-01-01T10:02:02Z",
        },
        "lang": "en",
    }


def test_revision_from_json(revision_data: str) -> None:
    revision_json = json.dumps(revision_data)
    revision = Revision.from_json(revision_json)

    for attr, expected in [
        (revision.id, 1234),
        (revision.bytes, 2800),
        (revision.lang, "en"),
        (revision.comment, "Added category baz."),
        (
            revision.timestamp,
            datetime.datetime(2022, 2, 15, 4, 30, 0, tzinfo=datetime.timezone.utc),
        ),
    ]:
        assert attr == expected

    for fn, arg, ret in [
        (len, revision.categories, 2),
        (len, revision.headings, 3),
        (len, revision.media, 0),
        (len, revision.links, 3),
        (len, revision.wikilinks, 1),
        (len, revision.references, 0),
        (len, revision.tags, 0),
    ]:
        assert fn(arg) == ret

    assert isinstance(revision.user, User)
    assert isinstance(revision.page, Page)
    assert isinstance(revision.parent, ParentRevision)


def test_revision_from_json_missing_properties(
    revision_data: Dict[str, Any],
) -> None:
    properties = list(revision_data.keys())
    for property in properties:
        model_field = Revision.model_fields.get(property)
        if model_field is not None and model_field.is_required():
            # remove current property which should raise
            # an error when this data is validated since
            # this property is required (without a default)
            value = revision_data.pop(property)
            with pytest.raises(InvalidJSONError):
                Revision.from_json(json.dumps(revision_data))

            # replace the removed property
            revision_data[property] = value
