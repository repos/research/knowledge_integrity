import asyncio
import json
import logging

from dataclasses import dataclass
from enum import Enum
from typing import Union

import mwapi

from knowledge_integrity.schema import Page, ParentRevision, Revision, User


class ErrorCode(Enum):
    PAGE_MISSING = "page_missing"
    USER_MISSING = "user_missing"
    PARENT_REVISION_MISSING = "parent_revision_missing"
    PARENT_REVISION_INFO_DELETED = "parent_revision_info_deleted"
    REVISION_MISSING = "revision_missing"
    REVISION_INFO_DELETED = "revision_info_deleted"


@dataclass(frozen=True)
class Error:
    code: ErrorCode
    response_body: str


async def get_user(session: mwapi.AsyncSession, user_id: int) -> Union[User, Error]:
    response = await session.get(
        action="query",
        formatversion=2,
        list="users",
        usprop="editcount|groups|registration",
        ususerids=user_id,
    )
    logging.debug("User: %d. Response: %r", user_id, response)
    user = response["query"]["users"][0]
    if user_id != 0 and "missing" in user:
        return Error(
            code=ErrorCode.USER_MISSING,
            response_body=json.dumps(response),
        )
    return User(id=user_id, **user)


async def get_page(session: mwapi.AsyncSession, page_id: int) -> Union[Page, Error]:
    response = await session.get(
        action="query",
        formatversion=2,
        prop="revisions",
        pageids=page_id,
        rvlimit=1,
        rvdir="newer",
        rvslots="main",
    )
    logging.debug("Page: %d. Response: %r", page_id, response)
    page = response["query"]["pages"][0]
    if "missing" in page:
        return Error(
            code=ErrorCode.PAGE_MISSING,
            response_body=json.dumps(response),
        )
    return Page(
        id=page_id,
        first_edit_timestamp=page["revisions"][0]["timestamp"],
        title=page["title"],
    )


async def get_parent_revision(
    session: mwapi.AsyncSession, rev_id: int, lang: str
) -> Union[ParentRevision, Error]:
    response = await session.get(
        action="query",
        formatversion=2,
        prop="revisions",
        revids=rev_id,
        rvprop="content|comment|size|tags|timestamp",
        rvslots="main",
    )
    logging.debug("Parent: %d (%s). Response: %r", rev_id, lang, response)
    if "badrevids" in response["query"]:
        return Error(
            code=ErrorCode.PARENT_REVISION_MISSING,
            response_body=json.dumps(response),
        )

    revision = response["query"]["pages"][0]["revisions"][0]
    if "texthidden" in revision["slots"]["main"]:
        return Error(
            code=ErrorCode.REVISION_INFO_DELETED,
            response_body=json.dumps(response),
        )
    return ParentRevision(
        id=rev_id,
        lang=lang,
        text=revision["slots"]["main"]["content"],
        **revision,
    )


async def get_revision(
    session: mwapi.AsyncSession, rev_id: int, lang: str
) -> Union[Revision, Error]:
    response = await session.get(
        action="query",
        formatversion=2,
        prop="revisions",
        revids=rev_id,
        rvprop="ids|content|comment|timestamp|size|userid|tags",
        rvslots="main",
    )
    logging.debug("Revision: %d (%s). Response: %r", rev_id, lang, response)
    if "badrevids" in response["query"]:
        return Error(
            code=ErrorCode.REVISION_MISSING,
            response_body=json.dumps(response),
        )

    page_id = response["query"]["pages"][0]["pageid"]
    revision = response["query"]["pages"][0]["revisions"][0]
    if "userhidden" in revision or "texthidden" in revision["slots"]["main"]:
        return Error(
            code=ErrorCode.REVISION_INFO_DELETED,
            response_body=json.dumps(response),
        )

    page, user, parent = await asyncio.gather(
        get_page(session, page_id),
        get_user(session, revision["userid"]),
        get_parent_revision(session, revision["parentid"], lang),
    )
    if isinstance(page, Error):
        return page
    if isinstance(user, Error):
        return user
    if isinstance(parent, Error):
        return parent

    return Revision(
        id=rev_id,
        lang=lang,
        page=page,
        parent=parent,
        user=user,
        text=revision["slots"]["main"]["content"],
        **revision,
    )
