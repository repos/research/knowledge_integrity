import re

from collections import defaultdict
from dataclasses import dataclass
from typing import Dict, List

import mwedittypes.utils
import mwparserfromhell as mwp

from mwtokenizer.tokenizer import Tokenizer
from unidecode import unidecode

from knowledge_integrity.models.reference_need.config import lang_sections_to_skip


min_sentence_length = 6
ref_tag = "[REF_FN_TOKEN]"
markup_regex = r"(<ref)|({{)|(\[\[)|(\()|(</ref>)|(/>)|(}})|(\]\])|(\))|(\")|('{2})"

CITATION_TEMPLATES = (
    "sfn",  # https://en.wikipedia.org/wiki/Template:Sfn
    "harv",  # https://en.wikipedia.org/wiki/Template:Harvard_citation
    "snf",  # https://en.wikipedia.org/wiki/Template:Snf
)


@dataclass
class SentenceInput:
    lang_code: str
    section_name: str
    sent: str
    prev_sent: str
    next_sent: str
    label: str


def wikitext_to_plaintext(wikitext: str, lang: str) -> str:
    """Convert wikitext to plaintext.

    Also removes text-formatting syntax i.e. italic (''), bold (''')
    and bold-italic (''''') from the text.
    """
    plaintext_tokens = []
    for node in mwp.parse(wikitext, skip_style_tags=True).nodes:
        node_text = mwedittypes.utils.extract_text(mwnode=node, lang=lang)
        if isinstance(node, mwp.nodes.Tag) and node.tag.lower() == "table":
            continue
        plaintext = re.sub("'{2,}", "", node_text)

        # `mwedittypes` returns an empty string for types it
        # does not currently handle like references and templates.
        # Since these types are important to us, we postprocess the output.
        if not plaintext:
            if isinstance(node, mwp.nodes.Tag):
                if node.tag.lower() == "ref":
                    plaintext = ref_tag
            elif isinstance(node, mwp.nodes.Template):
                template_name, template_params = node.name.lower(), node.params

                # Citation templates could have variations that slightly change
                # how a citation is transcluded e.g. harv and harvnb, so we only
                # check the prefix to catch all of those. See:
                # https://en.wikipedia.org/w/index.php?title=Template:Sfn#Other_author%E2%80%93date_citation_templates
                if any(template_name.startswith(t) for t in CITATION_TEMPLATES):
                    plaintext = ref_tag

                # If the node is the Referencing template i.e.:
                # https://en.wikipedia.org/wiki/Template:R
                elif template_name == "r":
                    plaintext = ref_tag

                # If the node is the Lang template i.e.:
                # https://en.wikipedia.org/wiki/Template:Lang
                elif template_name == "lang":
                    *_, text = template_params
                    plaintext = str(unidecode(text))

        plaintext_tokens.append(plaintext)

    return "".join(plaintext_tokens)


def _standardize_plaintext(text: str) -> str:
    # sentences within quotes ending with '."' are merged with the following sentence
    # on sentence tokenization
    return text.replace('."', '".').strip()


# https://github.com/AikoChou/citationdetective/tree/master
def _is_paired(markups: Dict[str, int]) -> bool:
    return all(
        (
            (markups["<ref"] == markups["</ref>"] + markups["/>"]),
            (markups["{{"] == markups["}}"]),
            (markups["[["] == markups["]]"]),
            (markups["("] == markups[")"]),
            (not markups['"'] % 2),
            (not markups["''"] % 2),
        )
    )


def _has_opening_markups(sentence: str) -> bool:
    opening_markup = ("<ref", "{{", "[[", "(", '"', "''")
    return any(markup in sentence for markup in opening_markup)


def _extract_paragraphs(wikitext: str, lang: str) -> List[str]:
    section_text = wikitext_to_plaintext(wikitext, lang)
    standardized_section = _standardize_plaintext(section_text)
    paragraphs = [
        p_stripped.replace(f".{ref_tag}", f"{ref_tag}.")
        for p in standardized_section.split("\n\n")
        if (p_stripped := p.strip())
    ]
    return paragraphs


def _format_sentence(sentence: str) -> str:
    sentence_without_refs = sentence.replace(ref_tag, "")
    stripped_sentence = mwp.parse(sentence_without_refs).strip_code().strip()
    return stripped_sentence


def extract_sentences(lang_code: str, wikitext: str) -> List[SentenceInput]:
    """Extract sentences from a given wikitext, considering markup,
    abbreviations and language-specific punctuation.
    """

    tokenizer = Tokenizer(language_code=lang_code)
    wikicode = mwp.parse(wikitext, skip_style_tags=True)
    sections_to_skip = set(
        sec.casefold() for sec in lang_sections_to_skip.get(lang_code, [])
    )

    sentences: List[SentenceInput] = []
    for section in wikicode.get_sections(levels=[2], include_lead=True):
        headings = section.filter_headings()
        section_name = headings[0].title.strip() if headings else "main_section"

        if section_name.casefold() in sections_to_skip:
            continue

        for heading in headings:
            section.remove(heading)

        for paragraph in _extract_paragraphs(
            wikitext=str(section.strip()), lang=lang_code
        ):
            is_opening = False
            broken_sent: List[str] = []
            markups: Dict[str, int] = defaultdict(int)
            prev_sent_unformatted = None
            for sent_idx, sent in enumerate(
                tokenizer.sentence_tokenize(paragraph, use_abbreviation=True)
            ):
                if is_opening:
                    for match in re.finditer(markup_regex, sent):
                        markups[match.group(0)] += 1
                    broken_sent.append(sent)
                    if _is_paired(markups):
                        sent = "".join(broken_sent)
                        is_opening = False
                        broken_sent.clear()
                        markups.clear()
                else:
                    if _has_opening_markups(sent):
                        for match in re.finditer(markup_regex, sent):
                            markups[match.group(0)] += 1
                        if not _is_paired(markups):
                            is_opening = True
                            broken_sent.append(sent)
                        else:
                            markups.clear()

                if is_opening or sent.strip()[0] in "|!<{":
                    continue

                # some cases like ellipsis or abbreviations (e.g., M.I.A.) are treated
                # as the end of a sentence. In this case we may produce false positives
                # by combining two actually separate sentences, but we will eliminate
                # more of incomplete sentecnes
                if prev_sent_unformatted and sent[0].islower():
                    sent = prev_sent_unformatted + sent
                    sentences.pop()

                sent_formatted = _format_sentence(sent)

                if sentences and sent_idx != 0:
                    prev_sent = sentences[-1].sent
                    sentences[-1].next_sent = sent_formatted
                else:
                    prev_sent = ""

                sentences.append(
                    SentenceInput(
                        lang_code=lang_code,
                        section_name=section_name.lower(),
                        sent=sent_formatted,
                        prev_sent=prev_sent,
                        next_sent="",
                        label="1" if ref_tag.lower() in sent.lower() else "0",
                    )
                )
                prev_sent_unformatted = sent

    return sentences
